﻿// Напишите программу, рассчитывающую сумму денег, которые вы получите при вложении начальной суммы с фиксированной процентной ставкой дохода через определенное количество лет. Пользователь должен вводить с клавиатуры начальный вклад, число лет и процентную ставку

#include "pch.h"
#include <iostream>
using namespace std;

int main()
{
	double vklad, let, proz, s, sum;
	setlocale(0, "");
	cout << "Введите начальный вклад: ";
	cin >> vklad;
	cout << " \nВведите число лет: ";
	cin >> let;
	cout << "\nВведите процентную ставку: ";
	cin >> proz;
	sum = vklad;
	for (int j = let; j > 0; j--)
	{
		s = sum / 100 * proz;
		sum = s + sum;
	}
	cout << "Через " << let << " лет вы получите " << sum << " долларов" << endl;
	system("pause");
	return 0;
}